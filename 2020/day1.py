numbers = []
while True:
    try:
        line = int(input())
    except:
        break
    numbers.append(line)

def part1(numbers, suma):
    dif={}
    for i, num in enumerate(numbers):
        n=suma-num
        if n not in dif:
            dif[num]=i
        else:
            return(n * num)

# print( part1(numbers, 2020) )
         
def part2(arr, arr_size, suma):
    for i in range( 0, arr_size-2):
        for j in range(i + 1, arr_size-1):
            for k in range(j + 1, arr_size):
                if arr[i] + arr[j] + arr[k] == suma:
                    return (arr[i] * arr[j] * arr[k])

print( part2(numbers, len(numbers), 2020) )