import re

passwords = []
while True:
    line = input()
        
    if(line != ""):
        passwords.append(line)
    else: 
        break

def part1(passwords):
    resultado = 0
    for password in passwords:
        
        #Formatear el input
        data = re.split('-| |: ', password)
        
        #Contar cuantas veces se repite el caracter
        repetidas = data[3].count( data[2] )
        
        if int(data[0]) <= repetidas <= int(data[1]):
            resultado += 1
        
    return resultado
    
# print( part1(passwords) )

# 1-3 a: abcde

def part2(passwords):
    resultado = 0
    for password in passwords:
        
        #Formatear el input
        data = re.split('-| |: ', password)
        
        first = int(data[0])
        second = int(data[1])
        caracter = data[2]
        passwdStr = data[3]
        
        #Validar pass
        if ( passwdStr[first - 1] == caracter ) is not ( passwdStr[second - 1] == caracter ):
            resultado = resultado + 1

    return resultado 
    
print( part2(passwords) )