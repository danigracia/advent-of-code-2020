import math

inputs = []
while True:
    try:
        line = int(input())
        inputs.append(line)
    except:
        break

total_fuel = 0
for n in inputs:
    fuel = math.floor(n / 3) - 2
    total_fuel += fuel
    
print(total_fuel)
    