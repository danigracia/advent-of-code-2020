numbers = []
while True:
    try:
        line = int(input())
    except:
        break
    numbers.append(line)


def part1():
    highers = 0
    for n in range(len(numbers)):
        old = numbers[ (n - 1) ]
    
    if(old < numbers[n]):
        highers += 1
    return highers

def part2():
    highers = 0
    for i in range( len(numbers) ):
        try:
            old_array = [ numbers[i - 1], numbers[i], numbers[i + 1] ]
            old_array_sum = 0
            for n in old_array:
                old_array_sum += n
            
            array = [ numbers[i], numbers[i + 1], numbers[i + 2] ]
            array_sum = 0
            for n in array:
                array_sum += n
        
            if(array_sum > old_array_sum):
                highers += 1
                print(array_sum, " is higher ", old_array_sum) 
        except:
            break
    return highers

        
print(part1())
print(part2())